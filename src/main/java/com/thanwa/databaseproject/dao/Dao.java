/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.databaseproject.dao;

import com.thanwa.databaseproject.model.User;
import java.util.List;

/**
 *
 * @author tud08
 */
public interface Dao<T> {
    T get(int id);
    List<T> getAll();
    T save(T obj);
    T update(T obj);
    int dalete(T obj);
    List<T> getAll(String where, String order);
}
