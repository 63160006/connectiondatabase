/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.databaseproject.service;

import com.thanwa.databaseproject.dao.UserDao;
import com.thanwa.databaseproject.model.User;

/**
 *
 * @author tud08
 */
public class UserService {
    public User login(String name, String password){
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if(user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
